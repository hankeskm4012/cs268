<%--
  Created by IntelliJ IDEA.
  User: Kevin
  Date: 4/21/2016
  Time: 10:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<c:url value='/resources/candy.js'/>"></script>

<html>
<head>
    <title>Login</title>
</head>
<body>
    <h1>
        Candy Login
    </h1>
    <c:if test="${not empty message}">
        <h3>
            ${message}
        </h3>
    </c:if>
    <form action="Login" method="post" onsubmit="return validateLogin();">
        <table>
            <tr>
                <td>
                    Username
                </td>
                <td>
                    <input type="text" name="username" id="username">
                </td>
            </tr>
            <tr>
                <td>
                    Password
                </td>
                <td>
                    <input type="password" name="password" id="password"><br>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <input type="submit" value="submit">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
