<%--
  Created by IntelliJ IDEA.
  User: Kevin
  Date: 4/25/2016
  Time: 5:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/candy.js'/>"></script>

<html>
<head>
    <title>Buy Candy!</title>
</head>
<body>
    <h1>
        Buy Candy!
    </h1>

    <form method="post" onsubmit="return validatePurchase();" action="${pageContext.request.contextPath}/Purchase">
        <table border="1">
            <tr>
                <td></td>
                <td>
                    Product
                </td>
                <td>
                    Price
                </td>
            </tr>
            <c:forEach items="${products}" var="product">
                <tr>
                    <td>
                        <input type="checkbox" name="prodIds[]" value="${product.name}">
                    </td>
                    <td>
                        ${product.description}
                    </td>
                    <td>
                        <fmt:formatNumber value="${product.price}" type="currency"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <input type="submit" value="Place Order">
    </form>
</body>
</html>
