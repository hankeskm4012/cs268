<%--
  Created by IntelliJ IDEA.
  User: Kevin
  Date: 4/25/2016
  Time: 6:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>You Bought Candy!</title>
</head>
<body>
    <h1>
        You Bought Candy!
    </h1>
    <table border="1">
        <tr>
            <td>
                Product
            </td>
            <td>
                Price
            </td>
        </tr>
        <c:forEach items="${products}" var="product">
            <tr>
                <td>
                        ${product.description}
                </td>
                <td>
                    <fmt:formatNumber value="${product.price}" type="currency"/>
                </td>
            </tr>
            <c:set var="orderTotal" value="${orderTotal + product.price}"/>
        </c:forEach>
    </table>
    <h3>
        Total Cost: <fmt:formatNumber value="${orderTotal}" type="currency"/>
    </h3>
</body>
</html>
