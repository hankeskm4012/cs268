function validatePurchase() {
    var products = document.getElementsByName('prodIds[]')
    for (i = 0; i < products.length; i++) {
        if (products[i].checked) {
            return true;
        }
    }
    alert("You didn't check any candy!");
    return false;
}

function validateLogin() {
    var username = document.getElementById("username");
    var password = document.getElementById("password");
    if (username.value.length == 0) {
        alert("You didn't supply a username!");
        username.focus();
        return false;
    }
    else if (password.value.length == 0) {
        alert("You didn't supply a password!");
        password.focus();
        return false;
    } else {
        return true;
    }
}