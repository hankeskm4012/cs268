package edu.uwec.controllers;

import edu.uwec.models.CandyProduct;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * assignment3 created by Kevin
 * on Apr.25.2016.
 */
@Controller
@RequestMapping("/Select")
public class SelectController {

    @RequestMapping(method = RequestMethod.GET)
    public String requireLogin(ModelMap model, HttpServletRequest request) {
        if (request.getSession().getAttribute("id") == null) {
            request.setAttribute("message", "Login required");
            return "login";
        }

        List<CandyProduct> candyProductList = new ArrayList<CandyProduct>();

        try {
            Context initContext = new InitialContext();
            Context envContext = (Context)initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource)envContext.lookup("jdbc/dariomysql");
            Connection conn = ds.getConnection();
            String sql = "SELECT prod_id, prod_desc, prod_price FROM candy_product ORDER BY prod_desc";
            PreparedStatement pstmt;
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                CandyProduct candyProduct = new CandyProduct();
                candyProduct.setDescription(rs.getString("prod_desc"));
                candyProduct.setName(rs.getString("prod_id"));
                candyProduct.setPrice(BigDecimal.valueOf(rs.getFloat("prod_price")));
                candyProductList.add(candyProduct);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("products", candyProductList);
        return "purchase";
    }
}
