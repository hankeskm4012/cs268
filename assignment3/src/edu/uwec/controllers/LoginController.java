package edu.uwec.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * assignment3 created by Kevin
 * on Apr.21.2016.
 */
@Controller
@RequestMapping("/Login")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public String hello(ModelMap model, HttpServletRequest request) {
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String Login(ModelMap model, HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        try {
            Context initContext = new InitialContext();
            Context envContext = (Context)initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource)envContext.lookup("jdbc/dariomysql");
            Connection conn = ds.getConnection();
            String sql = "SELECT cust_id FROM candy_customer WHERE username = ? AND password = ?";
            PreparedStatement pstmt;
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,username);
            pstmt.setString(2,password);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                request.getSession().setAttribute("id", "cust_id");
                return "redirect:/Select";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.getSession().removeAttribute("id");
        model.addAttribute("message", "Invalid username and/or password");
        return "login";
    }
}
