package edu.uwec.models;

import java.math.BigDecimal;

/**
 * assignment3 created by Kevin
 * on Apr.25.2016.
 */
public class CandyProduct {
    private String name;
    private String description;
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
