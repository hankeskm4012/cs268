package edu.uwec.models;

import java.io.Serializable;

/**
 * Jeopardia created by Kevin
 * on May.16.2016.
 */
public class Answer implements Serializable {

    static final long serialVersionUID = 1L;

    private String userAnswer;
    private String realAnswer;
    private boolean isCorrect;
    private String question;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRealAnswer() {
        return realAnswer;
    }

    public void setRealAnswer(String realAnswer) {
        this.realAnswer = realAnswer;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
