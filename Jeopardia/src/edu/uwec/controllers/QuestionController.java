package edu.uwec.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.uwec.models.Answer;
import edu.uwec.models.Question;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * assignment3 created by Kevin
 * on Apr.21.2016.
 */
@Controller
public class QuestionController {

    private static int count = 0;
    private static Map<Integer, Answer> answerMap = new HashMap<Integer, Answer>();
    private static Question[] questions;
    private static final String ITALICS_REGEX = "</?i>";
    private static final String QUOTES_REGEX = "\"";

    public QuestionController() throws IOException {
        final URL jServiceURL = new URL("http://jservice.io/api/random?count=10");
        ObjectMapper mapper = new ObjectMapper();
        questions = mapper.readValue(jServiceURL, Question[].class);
        count = 0;
    }

    @RequestMapping(value="/question", method = RequestMethod.GET)
    public String getQuestion(ModelMap model, HttpServletRequest request) throws IOException {
        if (request.getParameter("count") != null) {
            final URL jServiceURL = new URL("http://jservice.io/api/random?count=10");
            ObjectMapper mapper = new ObjectMapper();
            questions = mapper.readValue(jServiceURL, Question[].class);
            count = 0;
            answerMap = new HashMap<>();
        }
        model.addAttribute("question", questions[count]);
        model.addAttribute("count", count);
        /**
         * TODO: This should use OWASP encoder in the jsp instead of regex replace
         */
        String realAnswer = questions[count].getAnswer();
        realAnswer = Pattern.compile(QUOTES_REGEX).matcher(realAnswer).replaceAll("");
        model.addAttribute("realAnswer", realAnswer);
        return "question";
    }

    @RequestMapping(value="/submit", method = RequestMethod.POST)
    public String submitAnswer(ModelMap model, HttpServletRequest request) {
        Answer answer = new Answer();
        answer.setUserAnswer(request.getParameter("userAnswer"));
        String realAnswer = request.getParameter("realAnswer");
        System.out.println("Real Answer before regex replace: " + realAnswer);
        realAnswer = Pattern.compile(ITALICS_REGEX).matcher(realAnswer).replaceAll("");
        answer.setRealAnswer(realAnswer);
        /**
         * TODO: Research https://en.wikipedia.org/wiki/Levenshtein_distance algorithm and apply to user answer vs real answer
         */
        if (answer.getRealAnswer().equalsIgnoreCase(answer.getUserAnswer())) {
            answer.setCorrect(true);
        } else {
            answer.setCorrect(false);
        }
        answer.setQuestion(request.getParameter("question"));
        answerMap.put(count, answer);
        System.out.println(answerMap.size());
        System.out.println("User Answer: " + answer.getUserAnswer());
        System.out.println("Player Answer: " + answer.getRealAnswer());
        for (int i=count; i<answerMap.size(); i++) {
            System.out.println("Answer Map count: " + i);
            System.out.println("Answer Map User Answer: " + answerMap.get(i).getUserAnswer());
            System.out.println("Answer Map Real Answer: " + answerMap.get(i).getRealAnswer());
            System.out.println("Answer Map Correct: " + answerMap.get(i).isCorrect());
        }
        System.out.println("Count: " + count++);
        if (count >= 5) {
            return "redirect:/answers";
        } else {
            return "redirect:/question";
        }
    }

    @RequestMapping(value="/answers", method = RequestMethod.GET)
    public String getAnswers(ModelMap model, HttpServletRequest request) {
        model.addAttribute("answers", answerMap);
        return "/answers";
    }

}
