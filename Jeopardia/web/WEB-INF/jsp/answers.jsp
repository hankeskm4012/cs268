<%--
  Created by IntelliJ IDEA.
  User: Kevin
  Date: 5/16/2016
  Time: 4:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/resources/css/normalize.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/skeleton.css'/>" rel="stylesheet">
<script src="<c:url value='/resources/js/jquery-2.2.3.min.js'/>"></script>
<script src="<c:url value='/resources/js/question.js'/>"></script>
<c:set var="count" value="0"/>


<html>
<head>
    <title>Jeopardia!</title>
</head>
<body>
    <div class="container center-text">
        <h1>
            Jeopardia!
        </h1>
        <h2>
            Answers:
        </h2>
        <c:forEach items="${answers}" var="answer">
            <c:set var="count" value="${count + 1}"/>
            <p>
                Question # ${count}: ${answer.value.question}
            </p>
            <p>
                Your Answer: ${answer.value.userAnswer}
            </p>
            <p>
                Correct Answer: ${answer.value.realAnswer}
            </p>
            <p class="big">
                <c:choose>
                    <c:when test="${answer.value.correct}"><span class="green">Correct!</span></c:when>
                    <c:otherwise><span class="red">Incorrect :(</span></c:otherwise>
                </c:choose>
            </p>
            <hr>
        </c:forEach>
        <form method="get" action="${pageContext.request.contextPath}/question">
            <input type="hidden" name="count" value="1">
            <input type="submit" value="Play Again?">
        </form>
    </div>

</body>
</html>
