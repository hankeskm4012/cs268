<%--
  Created by IntelliJ IDEA.
  User: Kevin
  Date: 4/21/2016
  Time: 10:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link href="<c:url value='/resources/css/normalize.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/skeleton.css'/>" rel="stylesheet">
<script src="<c:url value='/resources/js/jquery-2.2.3.min.js'/>"></script>
<script src="<c:url value='/resources/js/question.js'/>"></script>

<html>
<head>
    <title>Jeopardia!</title>
</head>
<body>
    <div class="container center-text">
        <h1>
            Jeopardia!
        </h1>
        <form method="post" action="${pageContext.request.contextPath}/submit">
            <h2>
                Question ${count + 1}
            </h2>
            <p>
                Category: ${question.category.title}
            </p>
            <p>
                Question Value: <span class="green">${question.value}</span>
            </p>
            <p>
                ${question.question}
            </p>
            <input type="text" name="userAnswer" id="focus">
            <input type="hidden" name="realAnswer" value="${question.answer}">
            <input type="hidden" name="question" value="${question.question}">
            <input type="submit" value="Answer">

        </form>
        <p>
            Hover below if you want to cheat...boo!
        </p>
        <p id="answer">
            ${question.answer}
        </p>
    </div>
</body>
</html>
