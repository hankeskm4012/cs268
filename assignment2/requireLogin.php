<?php session_start();
	session_cache_limiter();
	header("Pragma: no-cache");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Expires: Thu, 19 NOV 1981 08:52:00 GMT");
	$cust_id = $_SESSION["cust_id"];
	error_reporting(E_ALL);
	if (is_null($cust_id) == true) {
		header("Location:login.php?msg=RequireLogin");
		die();
	}
?>