<!DOCTYPE html>
<html>
	<head>
		<title>Buy Candy!</title>
		<script src="candy.js" type="text/javascript"></script>
	</head>
	<body>
		<?php 
			include("requireLogin.php"); 
			include("darioDBConnection.php");

			$query = "SELECT prod_id, prod_desc, prod_price ".
					 "FROM candy_product ".
					 "ORDER BY prod_desc";

			try {
				$stmt = $db->query($query);
				$row_count = $stmt->rowCount();
			} catch(PDOException $e) {
				print "Error!: " . $e->getMessage() . "<br>";
				die();
			} ?>

		<h1>Buy Candy!</h1>	

		<form method="post" onsubmit="return validatePurchase();" action="purchaseInfo.php" >
			<table border="1">
				<tr>
					<td>
					</td>
					<td>
						Product
					</td>
					<td>
						Price
					</td>
				</tr>
				<?php foreach($stmt as $row) { ?>
				<tr>
				<td>
					<input type="checkbox" name="prodIds[]" value="<?php echo $row["prod_id"]; ?>">
				</td>
				<td>
					<?php echo $row["prod_desc"]; ?>
				</td>
				<td align="right">
					$<?php echo number_format($row["prod_price"], 2); ?>
				</td>
				</tr>
				<?php } ?>
			</table>
			<input type="submit" value="Place Order">
		</form>

			
		<?php $db = null; ?>	
	</body>
</html>		