<!DOCTYPE html>
<html>
	<head>
		<title>You Bought Candy!</title>
	</head>
	<body>
		<?php 
			include("requireLogin.php"); 
			include("darioDBConnection.php");

			$prodIds = "";
			foreach($_REQUEST["prodIds"] as $prodId) {
				$prodIds = $prodIds . $prodId . ",";
			}
			$prodIds = substr($prodIds, 0, -1);
			$query = "SELECT prod_desc, prod_price ".
					 "FROM candy_product ".
					 "WHERE prod_id in (".$prodIds.") ".
					 "ORDER BY prod_desc";

			try {
				$stmt = $db->query($query);
				$row_count = $stmt->rowCount();
			} catch(PDOException $e) {
				print "Error!: " . $e->getMessage() . "<br>";
				die();
			}

			$total = 0;
		?>

		<h1>You Bought Candy!</h1>

		<table border="1">
			<tr>
				<td>
					Product
				</td>
				<td>
					Price
				</td>
			</tr>
			<?php foreach($stmt as $row) { ?>
				<tr>
				<td>
					<?php echo $row["prod_desc"]; ?>
				</td>
				<td align="right">
					$<?php echo number_format($row["prod_price"], 2); 
					$total += $row["prod_price"]; ?>
				</td>
				</tr>
			<?php } ?>
		</table>

		<h3>Total Cost: 
			$<?php
			echo number_format($total, 2);
			?>
		</h3>

		<?php $db = null; ?>
	</body>
</html>			