<?php 
	session_start();
	include("darioDBConnection.php");
	
	$username = $_POST["username"];
	$password = $_POST["password"];

	$query = "SELECT cust_id ".
			 "FROM candy_customer ".
			 " WHERE username = :username AND password = :password";

	try {
		$stmt = $db->prepare($query);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':password', $password);
		$stmt->execute();
		$row_count = $stmt->rowCount();
	} catch(PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br>";
		die();
	}

	if ($row_count > 0) {
		header("Location:purchase.php");
		$row = $stmt->fetch();
		$_SESSION["cust_id"] = $row["cust_id"];
	} else {
		unset($_SESSION["cust_id"]);
		header("Location:login.php?msg=InvalidLogin");
	}

	$db = null;
?>