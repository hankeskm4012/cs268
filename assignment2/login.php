<!DOCTYPE html>
<html>
	<head>
		<title>Candy Login</title>
		<script src="candy.js" type="text/javascript"></script>
	</head>
	<body>
		<?php
		session_start();
		$error = $_REQUEST["msg"];

		?>

		<h1>Candy Login</h1>

		<?php if ($error == "InvalidLogin") { ?>
			<h3>Invalid username and/or password</h3>
		<?php } ?>

		<?php if ($error == "RequireLogin") { ?>
			<h3>Login required</h3>
		<?php } ?>

		<form name="login" method="post" onsubmit="return validateLogin();" action="processLogin.php" >
			<table>
				<tr>
					<td>
						Username
					</td>
					<td>
						<input type="text" name="username" id="username">
					</td>
				</tr>
				<tr>
					<td>
						Password
					</td>
					<td>
						<input type="password" name="password" id="password"><br>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<input type="submit" value="submit">
					</td>
				</tr>
			</table>
		</form>
		<br>
	</body>
</html>