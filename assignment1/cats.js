$(document).ready(function() {
	/* Can't get pre-loading to work properly...why?!?!? */
	$.preloadImages = function() {
		for (var i = 0; i < arguments.length; i++) {
			$("<img/>").attr("src", arguments[i]);
		}
	}

	$.preloadImages("images/cat1-large.jpg", "images/cat2-large.jpg", "images/cat3-large.jpg", "images/cat4-large.jpg", 
					"images/cat5-large.jpg", "images/cat6-large.jpg", "images/cat7-large.jpg", "images/cat8-large.jpg");

	$(".audioBoxBlack a").click(function() {
		document.getElementById('catMeow').play();
	});	

	$(".audioBoxWhite a").click(function() {
		document.getElementById('catMeow2').play();
	});

	$("#index .imageBox img").mouseover(function() {
		original = path = $(this).attr("src");
		var filteredImagePath = path.substring(0, path.length - 4) + '-filtered.jpg';
		$(this).attr("src", filteredImagePath);
	});

	$("#index .imageBox img").mouseout(function() {
		$(this).attr("src", path);
	});

	$("#nyan_cat").click(function() {
		$(document.getElementById("catVideo")).attr({src:"videos/nyan_cat.mp4", width:500, height:333, autoplay:true, loop:"loop"});
		$("#container").css('opacity', .3);
		$("#popupContainer").css('top',  ($(window).height() / 2) - (333 / 2) + $(window).scrollTop());
		$("#popupContainer").css('left', ($(window).width() / 2) - (500 / 2));
		$("#popupContainer").css('visibility', 'visible');;
		$("#container").css('pointer-events', 'none');
	});

	$("#popupContainer").click(function() {
		var video = document.getElementById("catVideo");
		video.pause();
		$(video).attr({width:0, height:0});
		$("#popupContainer").css('visibility', 'hidden');
		$("#container").css('opacity', 1);
		$("#container").css('pointer-events', 'auto');
	});

	$(".image").click(function() {
		var path = $(this).attr("src");
		var largeImagePath = path.substring(0, path.length-4) + '-large.jpg';
		$("#largeImage").attr("src", largeImagePath);
		// $("#largeImage").load(function() {
			$("#popupContainer").css('top',  ($(window).height() / 2) - ($("#largeImage").height() / 2) + $(window).scrollTop());
			$("#popupContainer").css('left', ($(window).width() / 2) - ($("#largeImage").width() / 2));
			$("#container").css('opacity', .3);
			$("#popupContainer").css('visibility', 'visible');
			$("#container").css('pointer-events', 'none');
		// });
	});

	$("#popupContainer").click(function() {
		$("#popupContainer").css('visibility', 'hidden');;
		$("#container").css('opacity', 1);
		$("#container").css('pointer-events', 'auto');
		$("#largeImage").attr("src", "");
	});

});